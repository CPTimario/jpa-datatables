package com.github.cptimario.datatables.components;

public enum JoinType {
    CROSS_JOIN, LEFT_JOIN
}
