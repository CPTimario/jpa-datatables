package com.github.cptimario.datatables.components;

public enum QueryType {
    RESULT_LIST, TOTAL_COUNT, FILTERED_COUNT
}
